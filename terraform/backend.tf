terraform {
  backend "s3" {
    bucket = "build-landing-aws-jenkins-terraform-yg"
    key = "lyg-node-aws-jenkins-terraform.tfstate"
    region = "ap-northeast-2"
  }
}

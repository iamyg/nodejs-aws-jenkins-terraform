terraform {
  backend "s3" {
    bucket = "build-landing-aws-jenkins-terraform-yg"
    key    = "jenkins.terraform.tfstate"
    region = "ap-northeast-2"
  }
}


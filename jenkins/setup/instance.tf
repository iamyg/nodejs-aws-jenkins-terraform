provider "aws" {
  region                  = "ap-northeast-2"
  shared_credentials_file = "/Users/gkohli/.aws/credentials"
}

resource "aws_instance" "yg-jenkins-instance" {
  ami           = var.AMIS[var.AWS_REGION]
  instance_type = "t2.micro"

  # the VPC subnet
  subnet_id = aws_subnet.yg-main-public-1.id

  # the security group
  vpc_security_group_ids = [aws_security_group.yg-jenkins-securitygroup.id]

  # the public SSH key
  key_name = aws_key_pair.yg-mykeypair.key_name

  # user data
  user_data = data.template_cloudinit_config.cloudinit-jenkins.rendered

  iam_instance_profile = aws_iam_instance_profile.yg-Jenkins-iam-role-instanceprofile.name
}

resource "aws_ebs_volume" "yg-jenkins-data" {
  availability_zone = "ap-northeast-2a"
  size              = 20
  type              = "gp2"
  tags = {
    Name = "jenkins-data"
  }
}

resource "aws_volume_attachment" "yg-jenkins-data-attachment" {
  device_name  = var.INSTANCE_DEVICE_NAME
  volume_id    = aws_ebs_volume.yg-jenkins-data.id
  instance_id  = aws_instance.yg-jenkins-instance.id
  skip_destroy = true
}

output "jenkins-ip" {
  value = [aws_instance.yg-jenkins-instance.*.public_ip]
}

